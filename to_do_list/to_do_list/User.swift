//
//  User.swift
//  to_do_list
//
//  Created by admin on 5.02.21.
//

import Foundation
import Firebase

struct Users {
    let uid: String
    let email: String
    
    init(user: User){
        self.uid = user.uid
        self.email = user.email!
    }
}
