//
//  ViewController.swift
//  to_do_list
//
//  Created by admin on 4.02.21.
//

import UIKit
import Firebase
class LoginViewController: UIViewController {

    
    var ref: DatabaseReference!
    
    @IBOutlet weak var warLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference(withPath: "users")
        self.registerForKeyboardNotifications()
        self.warLabel.alpha = 0
        Auth.auth().addStateDidChangeListener { [weak self](auth, user) in
            if user != nil{
                self?.performSegue(withIdentifier: "tasksSegue", sender: nil)
            }
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.emailTextField.text = ""
        self.passwordTextField.text = ""
    }

    private func registerForKeyboardNotifications() {
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: UIResponder.keyboardWillShowNotification, object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardHide), name: UIResponder.keyboardWillHideNotification, object: nil)
       }
   
    
    @objc private func keyboardDidShow(notification: Notification){
        guard let userInfo = notification.userInfo else {return}
        let kbFrameSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        (self.view as! UIScrollView).contentSize = CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.height + kbFrameSize.height)
        (self.view as! UIScrollView).scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: kbFrameSize.height, right: 0)
    }

   @objc private func keyboardHide(){
    (self.view as! UIScrollView).contentSize = CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.height)
    }
    
    
    func displayWarningLabet(withText text: String){
        self.warLabel.text = text
        UIView.animate(withDuration: 3, delay: 0, options:[.curveEaseInOut]) { [weak self] in
            self?.warLabel.alpha = 1
        } completion: { [weak self] (complete) in
            self?.warLabel.alpha = 0
        }

    }
    

    @IBAction func loginButton(_ sender: UIButton) {
        guard let email = self.emailTextField.text, let password = self.passwordTextField.text, email != "", password != "" else{
            self.displayWarningLabet(withText: "Info is incorrect")
            return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { [weak self] (user, error) in
            if error != nil{
                self?.displayWarningLabet(withText: "Error occured")
                return
            }
            
            if user != nil{
                self?.performSegue(withIdentifier: "tasksSegue", sender: nil)
                return
            }
            self?.displayWarningLabet(withText: "No such user")
        }
    }
    
    @IBAction func registerButton(_ sender: UIButton) {
        guard let email = self.emailTextField.text, let password = self.passwordTextField.text, email != "", password != "" else{
            self.displayWarningLabet(withText: "Info is incorrect")
            return
        }
        Auth.auth().createUser(withEmail: email, password: password) {[weak self] (user, error) in
            
            guard error == nil, user != nil else{
                print(error!.localizedDescription)
                return
            }
            let userRef = self?.ref.child((user?.user.uid)!)
            userRef?.setValue(["email": user?.user.email])
        }
    }
    
    
    
    
    
    
}

